# Xubuntu

## Install Programs

```
sudo apt-get purge -y light-locker parole
pkill light-locker
sudo add-apt-repository -y ppa:linrunner/tlp && sudo add-apt-repository -y ppa:numix/ppa && sudo apt update && sudo apt-get install -y cabextract curl clementine encfs geany filezilla keepassxc kodi network-manager-openvpn-gnome tlp numix-icon-theme numix-icon-theme-circle suckless-tools ttf-mscorefonts-installer xautolock gparted mpv && sudo tlp start && xfconf-query --channel thunar --property /misc-exec-shell-scripts-by-default --create --type bool --set true && sudo apt-get autoclean -y && sudo apt-get autoremove -y
```

The above commands:

- remove light-locker and replace it with slock
- remove parole (replace it with mpv)
- install: cabextract, curl, clementine, encfs, geany, filezilla, keepassxc, kodi, network-manager-openvpn-gnome, tlp, numix icon theme (+ circle icons), slock, xautolock, gparted, mpv
- set `misc-exec-shell-scripts-by-default` in Thunar to true to allow for executable shell scripts
- removes any unneeded packages

### Manually Install Other Programs

#### Standard Notes

<https://github.com/standardnotes/desktop/releases>

#### Riot

```
sudo sh -c "echo 'deb https://riot.im/packages/debian/ bionic main' > /etc/apt/sources.list.d/matrix-riot-im.list"
curl -L https://riot.im/packages/debian/repo-key.asc | sudo apt-key add -
sudo apt-get update && sudo apt-get -y install riot-web
```

- `Settings` > `Preferences` > enable `Start automatically after system login`

#### TeamViewer

<https://www.teamviewer.com/en/download/linux/>

- Configure:
  - General: assign to your account (`Grant easy access` ticked)
  - Security:
    - Random password > `Disabled (no random password)`
    - Black and whitelist > tick `Allow access only for the following partners` > type your account email address in `Whitelisted partners` and click on `+`

#### Discord

<https://discordapp.com/api/download?platform=linux&format=deb>

- To have Discord start on system boot, go into `User Settings > Linux Settings` and disable + enable `Open Discord`

#### Dropbox

<https://www.dropbox.com/install-linux>

#### Syncthing + Syncthing-GTK

Download `syncthing-linux-amd64-vx.x.x.tar.gz` from <https://github.com/syncthing/syncthing/releases> (`syncthing-linux-386-vx.x.x.tar.gz` if 32-bit) and place the `syncthing` file in `~/.local/bin` (open File Manager and press `CTRL+H` to toggle hidden files)

Then download the latest .AppImage release of Syncthing-GTK from <https://github.com/syncthing/syncthing-gtk/releases>

Handy command to run to delete any file conflicts: `find /home/username/Sync -name '*sync-conflict*' -delete`

## Configuration

### Appearance

`Appearance`:

- `Style` > `Numix`
- `Icons` > `Numix Circle`

`Window Manager`:

- `Style` > `Numix`
- `Keyboard` > `Show desktop` to `Alt+D`

`Window Manager Tweaks`:

- `Compositor` > untick `Enable display compositing`
- `Accessibility` > `Key used to grab and move windows` > change from Alt to Control (this is to be able to use the Clone Tool in Photoshop)

Changing desktop background:

1. Right-click on your desktop and click on `Desktop Setings...`
2. Set `Style` to `None`
3. Adjust `Color`

### Panel

Go to `Panel > Panel Preferences` and click on the `Items` tab.

- Whisker Menu:
  - `Behavior` > tick `Switch categories by hovering`
- Window Buttons:
  -  untick `Show handle`
  - `Sorting order` > `None, allow drag-and-drop`
  - `Middle click action` > `Close Window`
- Clock:
  - `Custom Format` > `%d %b, %H:%M:%S`

### Power Management

Click on the power manager icon in the system tray and click on `Power manager settings...`:

- `Display` > set `Switch off after` to `10 minutes` (`Blank after` and `Put to sleep after` will be `8` and `9 minutes` respectively)

### Startup

In `Settings > Sessions and Startup > Application Autostart` create new entries:

- KeepassXC: `keepassxc`
- xautolock: `xautolock -time 5 -locker slock`
- Standard Notes: *browse to the .AppImage file*
- Syncthing-GTK: *browse to the .AppImage file*

### Keyboard Shortcuts

In `Settings > Keyboard > Application Shortcuts` modify the existing entries:

- `mousepad`:  `Super+N`
- `exo-open --launch FileManager`:  `Super+E`
- `exo-open --launch TerminalEmulator`:  `Super+R`
- `xfce4-popup-whiskermenu`: `Super L`
- `xfce4-screenshooter`: `Print` (change from `xfce4-screenshooter -f`)
- `xflock4`: `Alt+L`
- `xflock4`: `Ctrl+Alt+L`

Add the following entries:

- `clementine -f`: `Alt+.`
- `clementine -r`: `Alt+,`
- `clementine -t`: `Alt+/`

### Add VPN Profile(s) (optional)

1. Click on the network icon in the system tray and click on `Edit Connections...`
2. Click on the `+` icon
3. In the dropdown select `Import a saved VPN configuration...` and click `Create...`
4. Select the .ovpn file
5. Configure the username/password (if needed)

### Mount encfs Shortcut (optional)

Create a new .sh file on your desktop with the following code:

```bash
#!/bin/bash
xfce4-terminal --command "encfs /home/username/Sync /home/username/crypt" --hold
```

Save the file and then right-click, `Properties...`, `Permissions` tab > tick `Allow this file to run as a program`

The above code assumes your encrypted encfs files are stored in `/home/username/Sync`

## Install Fonts

1. Create a new folder in your Home folder named `.fonts`
2. Copy any font files into this folder
3. Run the following command: `sudo fc-cache -f`

## Software Configuration

### Transmission

- `Edit` > `Preferences` > `Downloading` > untick `Show the Torrent Options dialogue`

### GitKraken

- `File` > `Preferences` > untick `Send usage data about GitKraken to Axosoft` and `Send bug reports about GitKraken to Axosoft`

### KeePassXC

- `Tools` > `Settings` > tick `Show a system tray icon`, `Hide window to system tray when minimized`, `Hide window to system tray instead of app exist`
- `Tools` > `Settings` > `Auto-Type` > set `Global Auto-Type shortcut` to `Ctrl+Alt+A`

### Standard Notes

- `File` > `Backups` > `Disable Automatic Backups`

## Install Wine (optional)

Run the following commands:

```
sudo dpkg --add-architecture i386
wget -nc https://dl.winehq.org/wine-builds/winehq.key
sudo apt-key add winehq.key
sudo apt-add-repository 'deb https://dl.winehq.org/wine-builds/ubuntu/ bionic main'
sudo apt-get install -y --install-recommends winehq-stable
rm winehq.key
```

Check <https://wiki.winehq.org/Debian> for the latest commands.

And to install `winetricks`:
```
wget  https://raw.githubusercontent.com/Winetricks/winetricks/master/src/winetricks && chmod +x winetricks && sudo mv -v winetricks /usr/bin
```

Once everything is installed, run `winecfg` in Terminal to initialize Wine. Download any dependencies when prompted.

If interested in installing a Windows program, refer to <https://appdb.winehq.org/index.php>.

### Configure Wine for Adobe Products

Run: `winetricks adobeair atmlib gdiplus msxml3 msxml6 vcrun2005 vcrun2008 ie6 fontsmooth=rgb`

Install the Adobe products you want (32-bit versions work best).

Once completed, create new .sh files on your desktop for each program. Example (change `username` to your username):

**Illustrator**
```
#!/bin/bash
xfce4-terminal --command "wine '/home/username/.wine/drive_c/Program Files (x86)/Adobe/Adobe Illustrator CS6/Support Files/Contents/Windows/Illustrator.exe'"
```

**Photoshop**
```
#!/bin/bash
xfce4-terminal --command "wine '/home/username/.wine/drive_c/Program Files (x86)/Adobe/Adobe Photoshop CS6/Photoshop.exe'"
```

Set the above .sh file(s) to be executable (chmod +x) or through the file property dialog.

If you have any issues running Illustrator you may need to install `ttf-mscorefonts-installer`: `sudo apt install ttf-mscorefonts-installer`

To use the Clone Tool in Photoshop, ensure `Key used to grab and move windows` in `Windows Manager Tweaks` > `Accessibility` is not set to `Alt`.

## Install LAMP (optional)

If you want to have your own local web/database server:

```
sudo apt-get install lamp-server^ phpmyadmin
```

When prompted, select `Apache` and leave the password blank for phpMyAdmin (to generate a random password).

Run the following command in terminal:

```
sudo mysql
ALTER USER 'root'@'localhost' IDENTIFIED WITH mysql_native_password BY 'root';
```

If setting up a locally accessible MySQL database (e.g. a shared Kodi MySQL database), in `/etc/mysql/mysql.conf.d/mysqld.cnf` set `bind-address` to your computer's IP address (e.g. `192.168.1.211`)

If setting up a cron job to automate MySQL database actions (e.g. mysqldump for regular backups) and you don't want to be prompted for the password, create a new file `~/.my.cnf`:

```
[mysqldump]
user=root
password=root
```

Example cron job: `0 6 * * * /home/username/Documents/mysqlback.sh` (runs daily at 6:00am)

`mysqlback.sh`:

```bash
#!/bin/bash
DATE=`date "+%Y%m%d"`
mysqldump MyVideos99 | gzip -c > /home/username/Dropbox/myvideos99-$DATE.sql.gz
find /home/username/Dropbox -mtime +7 -type f -delete
```

Save the file and then right-click, `Properties...`, `Permissions` tab > tick `Allow this file to run as a program`

### Create MySQL User Account

Run the following command either in the terminal (`sudo mysql`) or via phpMyAdmin (typically <http://127.0.0.1/phpmyadmin>, root:root):

```
CREATE USER 'username' IDENTIFIED BY 'password';
GRANT ALL ON *.* TO 'username';
FLUSH PRIVILEGES;
```
