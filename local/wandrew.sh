#!/bin/sh
### Modification of: https://www.snbforums.com/threads/wan_connection-isps-dhcp-did-not-function-properly.56226/page-3#post-515509
### Credits to:      Allan Marcus
### License:         GPL version 2.x or above
### ---
# Purpose:      Perform 3 tests (WAN IP, WAN state, and 3 ping tests), if any fail then release/renew DHCP + restart WAN
# Assumptions:  Asuswrt-Merlin firmware with custom scripts enabled, no dual-wan - just one WAN to rule them all.
# Installation: curl --retry 3 "https://gitlab.com/andryou/andrews-settings/raw/master/local/wandrew.sh" -o "/jffs/scripts/wandrew" && chmod 755 "/jffs/scripts/wandrew" && sh /jffs/scripts/wandrew enable
# Usage:        wandrew {enable|check|disable}

Say(){
	echo $$ "$@" | logger -st "($(basename "$0"))"
}

Is_Private_IP() {
	grep -oE "(^127\.)|(^(0)?10\.)|(^172\.(0)?1[6-9]\.)|(^172\.(0)?2[0-9]\.)|(^172\.(0)?3[0-1]\.)|(^169\.254\.)|(^192\.168\.)"
}

CheckPing(){
	ping -q -c 1 -W 2 "$1" > /dev/null
	return $?
}

CheckStatus(){
	DNSHOSTS="8.8.8.8 1.1.1.1 9.9.9.9"
	ERROR_IP=0
	ERROR_PING=0
	IPADDRESS=$(ifconfig eth0 | grep 'inet addr' | cut -d: -f2 | awk '{print $1}')
	WANSTATE=$(nvram get wan0_auxstate_t)
	if [ -z "$IPADDRESS" ] || [ "$WANSTATE" -ne "0" ] || [ -n "$(echo "$IPADDRESS" | Is_Private_IP)" ]; then
		ERROR_IP=1
	fi
	if [ $ERROR_IP -eq "0" ]; then
		UP=0
		for DNS in $DNSHOSTS; do
			if CheckPing "$DNS"; then
				UP=1
				break
			fi
		done
		if [ $UP -eq "0" ]; then
			ERROR_IP=1
			ERROR_PING=1
		fi
	fi
	if [ $ERROR_IP -eq "1" ]; then
		Say "ERROR ~ IP address = $IPADDRESS, WAN state = $WANSTATE (should be 0), ping = $ERROR_PING (should be 0)..."
	elif [ -n "$1" ] && [ "$1" -eq "1" ]; then
		Say "SUCCESS ~ IP address = $IPADDRESS, WAN state = $WANSTATE (should be 0)"
	fi
	return $ERROR_IP
}

case $1 in
	enable)
		if [ ! -f "/jffs/scripts/services-start" ]; then
			echo "#!/bin/sh" > /jffs/scripts/services-start
			echo >> /jffs/scripts/services-start
		elif [ -f "/jffs/scripts/services-start" ] && ! head -1 /jffs/scripts/services-start | grep -qE "^#!/bin/sh"; then
			sed -i '1s~^~#!/bin/sh\n~' /jffs/scripts/services-start
		fi
		if ! grep -qF "# wandrew" /jffs/scripts/services-start; then
			echo "/jffs/scripts/wandrew start # wandrew" >> /jffs/scripts/services-start
			chmod 0755 /jffs/scripts/services-start
		fi
		Say "ENABLED"
		cru a "wandrew" "*/5 * * * * /jffs/scripts/wandrew check"
	;;
	start)
		cru a "wandrew" "*/5 * * * * /jffs/scripts/wandrew check"
	;;
	check)
		if ! CheckStatus; then
			Say "RENEWING DHCP..."
			killall -USR1 udhcpc
			sleep 10
			if ! CheckStatus 1; then
				Say "RESTARTING WAN..."
				service restart_wan
			fi
		fi
	;;
	disable)
		cru d "wandrew"
		sed -i '\~# wandrew~d' /jffs/scripts/services-start
		Say "DISABLED"
	;;
	*)
		echo "Usage: wandrew {enable|check|disable}"
	;;
esac
