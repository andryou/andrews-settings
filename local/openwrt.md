# OpenWRT Configuration

The following configuration is intended for the <https://github.com/shunjou/openwrt-optimized-archer-c7-v2> build for TP-Link Archer C7 v2 routers.

Assumptions: dnsmasq-full, hd-idle, nfs-kernel-server, ntfs-3g already installed (which is the case currently with Shunjou's builds).

The configuration includes:
- Cloudflare DNS and Google DNS (primary, secondary respectively)
- two mount points for the 2 USB ports (assuming NTFS drives)
- no need for OpenVPN, simple-adblock and p910nd
- FTP access to router (vsftpd)
- SSH access to router

## Backup

If upgrading from an existing OpenWRT installation, backup the following files via FTP:

```
/etc/exports
/etc/rc.local
/etc/config/adblock
/etc/config/hd-idle
/etc/config/dhcp
/etc/config/wireless
/etc/config/sqm
/etc/crontabs/root
```

## Flash OpenWRT

**IMPORTANT: Connect your router to your computer via ethernet for this process.**

Download the latest `openwrt-ath79-generic-tplink_archer-c7-v2-squashfs-sysupgrade.bin` (if upgrading), or `https://github.com/shunjou/openwrt-optimized-archer-c7-v2/blob/master/openwrt-ath79-generic-tplink_archer-c7-v2-squashfs-factory.bin` (if flashing on factory TP-Link firmware) from  <https://github.com/shunjou/openwrt-optimized-archer-c7-v2>, upload it into the `/tmp` folder in your router and then and SSH in (if flashing from factory, rename .bin file to `openwrt.bin` and flash using web interface):

```
cd /tmp
sed -i 's#pci0000:01/0000:01:00.0#pci0000:00/0000:00:00.0#g; s#platform/qca955x_wmac#platform/ahb/ahb:apb/18100000.wmac#g' /etc/config/wireless
sysupgrade -F -n openwrt-ath79-generic-tplink_archer-c7-v2-squashfs-sysupgrade.bin
```

## Web Log In

1. Wait for the firmware to be flashed and then try connecting to `http://192.168.1.1` in your web browser
2. Log in (there won't be any set password)
3. Set a password
4. Configure the timezone, system name, theme (Bootstrap)
5. Enable SSH access for LAN
6. Configure wireless (refer to `/etc/config/wireless`)
    - configure SSID, security (`WPA2-PSK, CCMP`), channel (N: `1`, `6`, or `11`; AC: any relatively un-used channel), width (N: `20 MHz`; AC: `80 MHz`), transmit power (auto)
	- use mobile app like <https://f-droid.org/en/packages/com.vrem.wifianalyzer/>
7. Configure custom startup (refer to `/etc/rc.local`)

Sample custom startup commands to mount USB drive(s):

```
sleep 10
ntfs-3g /dev/sda1 /mnt/usb1 -o defaults,async,nofail,noatime
sleep 5
ntfs-3g /dev/sdb1 /mnt/usb2 -o defaults,async,nofail,noatime
```

## If No Backed Up Configs

Create `/etc/exports`:

```
/mnt/usb1 192.168.1.0/255.255.255.0(ro,no_root_squash,insecure,no_subtree_check,async,mp)
/mnt/usb2 192.168.1.0/255.255.255.0(ro,no_root_squash,insecure,no_subtree_check,async,mp)
```

Note: the above assumes the router IP address is `192.168.1.1`.
To change router IP address, SSH in and run `nano /etc/config/network`, change the IP address, save, and run `/etc/init.d/network restart`

Modify `/etc/config/hd-idle`:
```
config 'hd-idle'
	option 'disk' 'sda'
	option 'enabled' '1'
	option 'idle_time_unit' 'minutes'
	option 'idle_time_interval' '30'
```

### SQM

1. Go to <http://www.speedtest.net/> and do a speedtest.
2. Once it's completed, let's do some math:
    - Download Speed = `Speedtest Download Speed x 1000 x 0.95`
    - Upload Speed = `Speedtest Upload Speed x 1000 x 0.95`
3. Hover over `Network` and click on `SQM QoS`.
4. Tick the `Enable this SQM instance` box and ensure the `Interface name` is set to `eth0.2`
5. Set the Download and Upload speeds to what we calculated in step #2.
6. Make other configuration changes (`cake` + `piece_of_cake`, `nat dual-dsthost` (ingress), `nat dual-srchost` (egress))
6. Click on the `Save & Apply` button at the bottom of the page.

You can experiment with different settings, such as setting Download Speed to `0` to have SQM only for uploads.

## SSH

SSH into your router (you may need to clear previously stored credentials) and run the following commands:

```
opkg update
touch /etc/config/fstab
opkg install vsftpd
mkdir -p /mnt/usb1
mkdir -p /mnt/usb2
/etc/init.d/nfsd enable
service simple-adblock disable
service p910nd disable
service openvpn disable
```

Note: the above disables `OpenVPN`, `p910nd`, `simple-adblock`. Be selective about the above commands if you wish to use any of those.

### DNS

1. Connect via FTP and open `/etc/config/network`
2. Modify the wan and wan6 interfaces:
```
config interface 'wan'
	option ifname 'eth0.2'
	option proto 'dhcp'
	option peerdns '0'
	option dns '1.1.1.1 8.8.8.8'

config interface 'wan6'
	option ifname 'eth0.2'
	option proto 'dhcpv6'
	option peerdns '0'
	option dns '2606:4700:4700::1111 2001:4860:4860:0:0:0:0:8888'
```

### Adblock

1. Download adblock (<https://downloads.lede-project.org/snapshots/packages/x86_64/packages/>) and luci-app-adblock (<https://downloads.lede-project.org/snapshots/packages/x86_64/luci/>) to your computer.
2. Upload both .ipk files to `/tmp` via FTP (rename them `adblock.ipk` and `luci-app-adblock.ipk` respectively)
3. Connect via SSH
4. Once logged in, run the following commands:
```
cd /tmp
opkg install adblock.ipk luci-app-adblock.ipk
/etc/init.d/adblock enable
```
5. Open `/etc/config/adblock`
6. Replace all instances of `option enabled '1'` with `option enabled '0'`
7. Add the below entry, save and re-upload:
```
config source 'stevenblack'
	option adb_src 'https://gitlab.com/andryou/andrews-settings/raw/master/hostslite'
	option adb_src_rset '/^0\.0\.0\.0[[:space:]]+([[:alnum:]_-]+\.)+[[:alpha:]]+([[:space:]]|$)/{print tolower(\$2)}'
	option adb_src_desc 'unified blocklist, daily updates, approx. 23.000 entries'
	option enabled '1'
```
8. Add as scheduled task: `0 12 * * 0 /etc/init.d/adblock reload`

### Firewall => MSS Clamping

If you have Cable service, untick this option on the Firewall page.
If you have DSL service, it is recommended to keep it ticked (default).

## If Backed Up Configs

FTP into your router and upload the following files (overwriting any existing):

```
/etc/exports
/etc/config/adblock
/etc/config/hd-idle
/etc/config/sqm
/etc/crontabs/root
```

FTP in and edit `/etc/config/dhcp` and append dhcp static entries from the backed up `/etc/config/dhcp` file after the `config odhcpd 'odhcpd'` section

## Reboot

Reboot the router after all of the above.

Once rebooted, go to <https://dnsleaktest.com/> to verify you are using Cloudflare and/or Google DNS.
