# Linux Mint - Windows XP Luna

**Download: https://gitlab.com/andryou/andrews-settings/raw/master/local/linuxmint/WindowsXPFiles.zip**

## Fonts

1. Run: sudo mkdir /usr/share/fonts/truetype/msttcorefonts
2. Copy font files in the XP Media/Fonts folder to the above folder (you will need to navigate to it and open as root)
3. Run: sudo fc-cache -fv
4. Start Menu => Preferences => Font Selection
    - Tahoma (Default, Desktop, Document)
    - Trebuchet MS Bold (Window Title)
    - (optional) DejaVu Sans Mono Book (Monospace)
    - Font size for all: 9
    - Hinting: Full
    - Antialiasing: Greyscale

## Theme

1. Copy the ".icons" and ".themes" folders to your Home directory (press CTRL + H in the file explorer to toggle hidden folders)
2. Run: ln -s ~/.icons/cinnxp/cursors/ ~/.icons/default
3. Right-click on an empty space on your panel and click on "Themes"
    - Window Borders: xp
    - Icons: xp
    - Controls: cinnxp
    - Mouse Pointer: cinnxp
    - Desktop: cinnxp
4. Right-click on an empty space on your panel and click on "Panel settings"
    - Panel Height: 27px
    - Icon size (all): 16px
5. Right-click on an empty space on your panel and click on "Add applets to the panel"
    - Add applet "CinnVIIStarkMenu" from "Download" tab (update cache when asked)
    - Once downloaded, go to "Manage" tab, click on CinnVIIStarkMenu and press the + button
    - Right-click on your panel and switch "Panel edit mode" on
    - Drag and drop new menu applet to far left of panel
    - Right-click on the old menu and remove it
    - (optional) If you prefer to not have any launchers, right-click on launcher area (to the right of the start menu) and remove "Panel launchers" (or from the "Add applets to the panel" dialog)
6. Right-click on the new start menu and click on Configure...
    - Panel => Use a custom icon - on, then for Icon click on the icon button and navigate to /home/USERNAME/.themes/cinnxp/cinnamon/menu.png and select it (if you don't see the .theme folder, press CTRL + H to toggle hidden folders)
    - Set 'Menu'-Button label to: start
7. (optional) Configure Window List applet to disable thumbnails by right-clicking on a window button in your taskbar, Preferences, Configure... and setting "Show windows thumbnails on hover" to Off
8. Start => search for "effects" => disable window effects, fade effect, session startup animation
9. Copy XP Media/windows_xp_bliss.jpg to your Pictures folder then right-click on your desktop => Change Desktop Background => Pictures => select the Bliss wallpaper
10. Run: sudo apt install dconf-editor
11. Run: dconf-editor
12. Navigate to: org => gnome => nautilus => desktop => trash-icon-name, set "Use default value" to off and set custom value to "Recycle Bin"
13. Start => search for "desktop", enable Trash, disable Home
14. Copy applet.js to /usr/share/cinnamon/applets/window-list@cinnamon.org (you will need to do this as root)
    - alternatively you can comment out **title = "["+ title +"]";** in /usr/share/cinnamon/applets/window-list@cinnamon.org/applet.js by prepending it with: //

## Sounds:

1. Copy sound files in the XP Media/Sounds folder to any folder, I'd recommend creating a new subfolder under Music
2. Start => search for sound => "Sounds" tab:
    - Starting Cinnamon - Windows XP Startup.wav
    - Leaving cinnamon - Windows XP Shutdown.wav
    - Inserting a device - Windows XP Hardware Insert.wav
    - Removing a device - Windows XP Hardware Remove.wav
    - Showing notifications - Windows XP Balloon.wav
    - Changing the sound colume - Windows XP Ding.wav
3. Set all other sounds off
