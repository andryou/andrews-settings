# Andrew's Firefox Settings

## Add-ons

Note: you can also sign into Sync to automatically install any add-ons you have in another Firefox installation.

### Essential

- [uBlock Origin](https://addons.mozilla.org/en-CA/firefox/addon/ublock-origin/) - adblocker
- [Decentraleyes](https://addons.mozilla.org/en-CA/firefox/addon/decentraleyes/) - speed up page load times for most sites
- [HTTPS Everywhere](https://addons.mozilla.org/en-CA/firefox/addon/https-everywhere/) - automatically use the secure version of sites if available
- [Privacy Badger](https://addons.mozilla.org/en-CA/firefox/addon/privacy-badger17/) - a self-learning cookie tracker blocker
- [Tracking Token Stripper](https://addons.mozilla.org/en-CA/firefox/addon/utm-tracking-token-stripper/) - automatically remove common tracking tokens from addresses

### Handy

- [Auto Tab Discard](https://addons.mozilla.org/en-CA/firefox/addon/auto-tab-discard/) - put inactive tabs to sleep to save memory
- [Imagus](https://addons.mozilla.org/en-CA/firefox/addon/imagus/) - show thumbnail of any images on hover

### Site-Specific

- [Amazon Container](https://addons.mozilla.org/en-CA/firefox/addon/amazon-container/)
- [Facebook Container](https://addons.mozilla.org/en-CA/firefox/addon/facebook-container/)
- [F.B Purity](https://www.fbpurity.com/) - various tweaks for Facebook
- [Google Container](https://addons.mozilla.org/en-CA/firefox/addon/google-container/)
- [View Image](https://addons.mozilla.org/en-CA/firefox/addon/view-image/) - re-add the "View Image" button for Google Images
- [Reddit Container](https://addons.mozilla.org/en-CA/firefox/addon/contain-reddit/)
- [Reddit Enhancement Suite](https://addons.mozilla.org/en-CA/firefox/addon/reddit-enhancement-suite/) - various tweaks for Reddit
- [Twitter Container](https://addons.mozilla.org/en-CA/firefox/addon/twitter-container/)

## Configuration

### Browser Settings

Go to `Menu` => `Preferences`:

- General:
  - Browsing => `Use smooth scrolling` and `Search for text when you start typing` enabled, all others disabled
- Home:
  - Firefox Home Content => Untick `Highlights` and `Snippets`
- Search:
  - Default Search Engine => Untick `Show search suggestions ahead of browsing history in address bar results`
- Privacy & Security:
  - Content Blocking => `Standard`
  - Logins and Passwords => Untick `Ask to save logins and passwords for websites`
  - Permissions => Untick `Prevent accessibility services from accessing your browser`
  - Firefox Data Collection and Use => Untick everything
  
### Appearance
  
Go to `Menu` => `Customize`:

- Under `Toolbars` tick `Bookmarks Toolbar`
- Set `Themes` to `Dark`
- Set `Density` to `Compact`
- Drag-and-drop icons to/from the toolbar as needed. Icons I like to have in the toolbar are uBlock Origin and Privacy Badger.

### uBlock Origin

1. Click on the "uo" shield icon in the toolbar, and click on the mixer icon (right-most) below the big power icon
2. Click on the `Filter lists` tab
3. At the bottom tick `Import...` then paste in the following two links:

```
https://gitlab.com/andryou/andrews-settings/raw/master/hostslite
https://gitlab.com/andryou/andrews-settings/raw/master/ublock
```

4. Click on `Apply changes` in the top-right corner