#!/usr/bin/env python

#import math
from wrapper import *

target_portfolio = {
    'XUU.TO': 0.55,
    'XEF.TO': 0.25,
    'XEC.TO': 0.10,
    'VCN.TO': 0.10,
}

q = QuestradeWrapper()

# let's balance my account
accounts = q.accounts()
for account in accounts:
	# get the total equity in my account
	balances = q.accounts_balances(account)
	for balance in balances:
		if balance.currency == 'CAD':
			break
	total_equity = balance.totalEquity

	# get symbol information for target assets
	symbols = q.symbols(target_portfolio.keys())

	# get quotes for target assets
	quotes = q.markets_quotes(symbols)
	
	# get current positions in my account
	positions = q.accounts_positions(account)
	
	###### CALCULATIONS

	# calculate target equity by asset
	target_equity = {symbol: total_equity * target_portfolio[symbol] for symbol in target_portfolio}
	
	### NON-BUY ONLY

	# calculate target units by asset
	target_units = {quote.symbol: target_equity[quote.symbol] // quote.askPrice for quote in quotes}
	
	# calculate non buy-only trades
	trades = {position.symbol: int(target_units[position.symbol] - position.openQuantity) for position in positions}
	
	### BUY ONLY
		
	# get current market value for positions
	delta_equity = {position.symbol: (target_equity[position.symbol] - position.currentMarketValue) if (target_equity[position.symbol] - position.currentMarketValue) > 0 else 0 for position in positions}
		
	# get total delta sum
	delta_total = sum(delta_equity.values())
	
	# get ask prices
	ask_prices = {quote.symbol: quote.askPrice for quote in quotes}
	bid_prices = {quote.symbol: quote.bidPrice for quote in quotes}

	# calculate buy-only trades
	trades_buyonly = {delta: int(delta_equity[delta] / delta_total * balance.buyingPower / ask_prices[delta]) for delta in delta_equity}

	# print results
	print()
	print(' # %s' % account.type)
	print('--------------')
	print(' * Available Cash: '+"${0:,.2f}".format(balance.buyingPower))
	print()
	print(' * Buy-only:')
	if all([t == 0 for t in trades_buyonly.values()]):
		print(' > There is not enough cash to buy anything.')
	else:
		for symbol, units in sorted(trades_buyonly.items(), key=lambda x: x[1]):
			if units > 0:
				print(' > BUY: \t'+symbol+' \t'+str(units)+' units \t'+"${0:,.2f}".format(ask_prices[symbol]))
			elif units == 0:
				pass
	print()
	print(' * Non Buy-only:')
	if all([t == 0 for t in trades.values()]):
		print(' > Balanced!')
	else:
		for symbol, units in sorted(trades.items(), key=lambda x: x[1]):
			if units < 0:
				print(' > SELL: \t'+symbol+' \t'+str(abs(units))+' units \t'+"${0:,.2f}".format(bid_prices[symbol]))
			elif units == 0:
				pass
			else:
				print(' > BUY: \t'+symbol+' \t'+str(units)+' units \t'+"${0:,.2f}".format(ask_prices[symbol]))

