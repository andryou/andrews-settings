# Andrew's Settings (and Config Files)

This repo is more for me to keep my configuration files for various apps/programs in sync on various devices, but you may look at them if you like.

**/**:
- [ab-facebookmessages.txt](https://gitlab.com/andryou/andrews-settings/raw/master/ab-facebookmessages.txt) - adblock rules to hide read/typing on Facebook
- [andrewblocklist](https://gitlab.com/andryou/andrews-settings/raw/master/andrewblocklist) - personal blocklist with any nasty domains I come across
- [andrewblocklist-home](https://gitlab.com/andryou/andrews-settings/raw/master/andrewblocklist-home) - personal blocklist for my home network
- [cloudflare.html](https://gitlab.com/andryou/andrews-settings/blob/master/cloudflare.html) - daily checked CloudFlare .tld list
- [ebateswhitelist](https://gitlab.com/andryou/andrews-settings/raw/master/ebateswhitelist) - ebates.ca domain whitelist
- [rfdaffiliatedomains](https://gitlab.com/andryou/andrews-settings/raw/master/rfdaffiliatedomains) - RedFlagDeals affiliate domains (to whitelist if wanted)
- [rfdaffiliatedomainsabp](https://gitlab.com/andryou/andrews-settings/raw/master/rfdaffiliatedomainsabp) - RedFlagDeals affiliate domains (to whitelist if wanted) - Adblock Plus format
- [whitelist_pihole](https://gitlab.com/andryou/andrews-settings/raw/master/whitelist_pihole) - daily scrape of https://discourse.pi-hole.net/t/commonly-whitelisted-domains/212

**/local**:
- [adblock](https://gitlab.com/andryou/andrews-settings/raw/master/local/adblock) - https://github.com/openwrt/packages/tree/master/net/adblock/files
- [advancedsettings.xml](https://gitlab.com/andryou/andrews-settings/raw/master/local/advancedsettings.xml) - SPMC config file
- [firefox](https://gitlab.com/andryou/andrews-settings/blob/master/local/firefox.md) - Firefox config
- [linuxmint](https://gitlab.com/andryou/andrews-settings/blob/master/local/linuxmint.md) - Linux Mint config
- [openwrt](https://gitlab.com/andryou/andrews-settings/blob/master/local/openwrt.md) - TP-Link Archer C7 OpenWRT config
- [simple-adblock](https://gitlab.com/andryou/andrews-settings/raw/master/local/simple-adblock) - https://github.com/openwrt/packages/tree/master/net/simple-adblock/files
- [torbrowser.txt](https://gitlab.com/andryou/andrews-settings/raw/master/local/torbrowser.txt) - things I do after downloading a fresh copy of Tor Browser
- [wandrew.sh](https://gitlab.com/andryou/andrews-settings/raw/master/local/wandrew.sh) - a script for ASUS routers to check Internet status and release/renew DHCP, restart WAN, and/or reboot the router if a problem is detected
- [xubuntu](https://gitlab.com/andryou/andrews-settings/blob/master/local/xubuntu.md) - Xubuntu config

**/snippets**:
- random scripts/code
