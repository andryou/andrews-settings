import re
import subprocess
import os
import glob

# Run in folder with .srt and .mkv files, where they have matching S00E00 season/episode values
# Requirements: mkvtoolnix

def seasonep(str):
	str = os.path.basename(str)
	parsed = re.search(r"S(\d+)E(\d+)", str, flags=re.IGNORECASE)
	if parsed is not None:
		return parsed.group(0).upper()
	else:
		return None

currdir = os.path.dirname(os.path.realpath(__file__))
currdir = currdir.replace('[', '[[]')
subs = glob.glob(currdir+"/*.srt")
videos = glob.glob(currdir+"/*.mkv")
subs.sort()
videos.sort()
for sub in subs:
	subep = seasonep(sub)
	print("Processing "+sub+" (parsed: "+subep+")")
	for video in videos:
		if subep == seasonep(video):
			print(">>> Match found:")
			print("--- Subs: "+sub)
			print("--- Video: "+video)
			pathext = os.path.splitext(video)
			outfile = pathext[0]+'_'+pathext[1]
			# >>> Replace all existing subtitles in file with .srt file
			subprocess.call(['mkvmerge', '-o', outfile, '-S', video, '--language', '0:eng', '--default-track', '0:yes', sub])
			# >>> Clean up and replace old with new video
			os.remove(sub)
			os.remove(video)
			os.rename(outfile, video)
			videos.remove(video)
			break
