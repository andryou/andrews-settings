import requests
import re
import shutil
import os

# INSTRUCTIONS:
# 1) create a new txt file in the same folder as this script named flickrdata.txt
# 2) go to the flickr album to download, view page source and copy the code into flickrdata.txt
# 3) press F12 in the page, click on the Network tab, then click on XHR
# 4) scroll down the album page to ensure all images are loaded, then for each GET api.flickr.com rest?ext... request in the Network tab, click on the Response tab and copy the raw "Response payload" text into flickrdata.txt
# 5) once done, then you can run this script

def download_file(url):
	print("Downloading: "+url)
	local_filename = url.split('/')[-1]
	if os.path.isfile(local_filename):
		print(local_filename+" already exists, skipping!")
	else:
		r = requests.get(url, stream=True)
		with open(local_filename, 'wb') as f:
			shutil.copyfileobj(r.raw, f)
		print(local_filename+" downloaded!")

	return local_filename

fh = open("flickrdata.txt", "r")
flickr = fh.read()
fh.close()
matches = re.findall("\":\"([^\"]+)_o\.jpg\"", flickr)
if matches:
	images = []
	for match in matches:
		url = match.replace("\/", "/")+"_o.jpg"
		if (url[0] == "/"):
			url = "https:"+url
		if (url not in images):
			images.append(url)
	print(str(len(images))+" unique images found!")
	for image in images:
		download_file(image)
	fh = open("flickrurls.txt", "w")
	fh.write("\n".join(images))
	fh.close()
