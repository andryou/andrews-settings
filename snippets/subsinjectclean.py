import re
import subprocess
import os
import glob

# Run in folder with .srt and .mkv files, where they have matching S00E00 season/episode values
# Requirements: mkvtoolnix

toremove = "explosiveskull|mkvcage|addic7ed"

def seasonep(str):
	str = os.path.basename(str)
	parsed = re.search(r"S(\d+)E(\d+)", str, flags=re.IGNORECASE)
	if parsed is not None:
		return parsed.group(0).upper()
	else:
		return None

currdir = os.path.dirname(os.path.realpath(__file__))
currdir = currdir.replace('[', '[[]')
subs = glob.glob(currdir+"/*.srt")
videos = glob.glob(currdir+"/*.mkv")
subs.sort()
videos.sort()
for sub in subs:
	subep = seasonep(sub)
	print("Processing "+sub+" (parsed: "+subep+")")
	for video in videos:
		if subep == seasonep(video):
			print(">>> Match found:")
			print("--- Subs: "+sub)
			print("--- Video: "+video)
			pathext = os.path.splitext(video)
			outfile = pathext[0]+'_'+pathext[1]
			# >>> Remove tags in subtitles
			fh = open(sub, 'r')
			subtitles = fh.read()
			fh.close()
			subtitles = re.sub('\d+(?:\n|\r\n?)\d{2}:\d{2}:\d{2},\d{3} --> \d{2}:\d{2}:\d{2},\d{3}(?:\n|\r\n?)(?:.*(?:'+toremove+').*(?:\n|\r\n?).*|.*(?:(?:\n|\r\n?).*)?(?:'+toremove+').*)(?:\n|\r\n?){,2}', '', subtitles, flags=re.MULTILINE|re.IGNORECASE) # remove entire subtitle element containing words to remove
			#subtitles = re.sub('.*(?:'+toremove+').*(?:\n|\r\n?)', '', subtitles, flags=re.IGNORECASE) # only remove subtitle text, safer but less complete
			fh = open(sub, 'w')
			fh.write(subtitles)
			fh.close()
			# >>> Replace all existing subtitles in file with .srt file
			subprocess.call(['mkvmerge', '-o', outfile, '-S', video, '--language', '0:eng', '--default-track', '0:yes', sub])
			# >>> Clean up and replace old with new video
			os.remove(sub)
			os.remove(video)
			os.rename(outfile, video)
			videos.remove(video)
			break
