import re
import subprocess
import os
import glob

# Run in folder with softcoded .mkv files which have softcoded subtitles
# Note: looks for "English" tracks
# Requirements: mediainfo, mkvtoolnix

toremove = "explosiveskull|mkvcage|addic7ed"

nosubs = []
currdir = os.path.dirname(os.path.realpath(__file__))
currdir = currdir.replace('[', '[[]')
videos = glob.glob(currdir+"/*.mkv")
videos.sort()
for video in videos:
	videoname = os.path.basename(video)
	print(videoname)
	pathext = os.path.splitext(video)
	outfile = pathext[0]+'_'+pathext[1]

	info = subprocess.check_output(['mediainfo', video])
	#print(info)
	matches = re.findall(r"Text( #\d+)?\s+ID\s+:\s+(\d+)\s[\W\w]+?Count of elements\s+:\s+(\d+)\s[\W\w]+?Language\s+:\s+(\w+)", info.decode('utf-8'))
	if not matches:
		matches = re.findall(r"Text( #\d+)?\s+ID\s+:\s+(\d+)\s[\W\w]+?Language\s+:\s+(\w+)[\W\w]+?FromStats_FrameCount\s+:\s+(\d+)", info.decode('utf-8'))
	#print(matches)

	nosub = True
	nummatches = len(matches)
	if nummatches > 0:
		#print(">>> "+str(nummatches)+" total subtitle track(s) found")
		tracktoget = None
		maxlines = 0
		engtracks = 0
		for m in matches:
			if m[2].isdigit():
				count = int(m[2])
				lang = m[3]
			else:
				count = int(m[3])
				lang = m[2]
			if lang == "English":
				engtracks += 1
				if count > maxlines:
					maxlines = count
					tracktoget = m[1]
			print("--- Track ID #"+m[1]+" | Element Count: "+m[2]+" | Language: "+m[3])
		if engtracks > 0:
			nosub = False
			#print(">>> "+str(engtracks)+" English subtitle track(s) found")
			if nummatches > 1:
				if tracktoget:
					#print(">>> Track to get: #"+tracktoget)
					# >>> Extract subtitles
					subprocess.call(['mkvextract', 'tracks', video, str(int(tracktoget)-1)+':eng.srt'])
					# >>> Remove tags in subtitles
					subsfile = currdir+"/eng.srt"
					fh = open(subsfile, 'r')
					subtitles = fh.read()
					fh.close()
					subtitles = re.sub('\d+(?:\n|\r\n?)\d{2}:\d{2}:\d{2},\d{3} --> \d{2}:\d{2}:\d{2},\d{3}(?:\n|\r\n?)(?:.*(?:'+toremove+').*(?:\n|\r\n?).*|.*(?:(?:\n|\r\n?).*)?(?:'+toremove+').*)(?:\n|\r\n?){,2}', '', subtitles, flags=re.MULTILINE|re.IGNORECASE) # remove entire subtitle element containing words to remove
					#subtitles = re.sub('.*(?:'+toremove+').*(?:\n|\r\n?)', '', subtitles, flags=re.IGNORECASE) # only remove subtitle text, safer but less complete
					fh = open(subsfile, 'w')
					fh.write(subtitles)
					fh.close()
					# >>> Replace all existing subtitles in file with .srt file
					subprocess.call(['mkvmerge', '-o', outfile, '-S', video, '--language', '0:eng', '--default-track', '0:yes', subsfile])
					# >>> Clean up and replace old with new video
					os.remove(subsfile)
					os.remove(video)
					os.rename(outfile, video)
	if nosub:
		nosubs.append(videoname)
if len(nosubs) > 0:
	print("\n>>> "+str(len(nosubs))+" videos without any English subtitles:")
	print(*nosubs, sep = "\n")
else:
	print("\n>>> All videos have English subtitles!")
