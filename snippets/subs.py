import re
import subprocess
import os
import glob
import sys

# Run in folder with softcoded .mkv files which have multiple subtitle tracks
# Note: looks for "English" tracks
# Requirements: mediainfo, mkvtoolnix

reencode = False
prefaudio = "English"
	
if len(sys.argv) > 1:
	if sys.argv[1] == "e":
		prefaudio = "English"
	elif sys.argv[1] == "j":
		prefaudio = "Japanese"
	if sys.argv[2]:
		reencode = "forced reencode (audio: "+prefaudio+")"
	print(">>> Preferred Language: "+prefaudio)

activitylog = []
nosubs = []
currdir = os.path.dirname(os.path.realpath(__file__))
currdir = currdir.replace('[', '[[]')
videos = glob.glob(currdir+"/*.mkv")
videos.sort()
for video in videos:
	videoname = os.path.basename(video)
	if "'" in videoname:
		os.rename(video, os.path.dirname(video)+'/'+videoname.replace("'", ""))
		video = os.path.dirname(video)+'/'+videoname.replace("'", "")
		videoname = os.path.basename(video)
	print(videoname)
	subprocess.call(['mkvpropedit', '--add-track-statistics-tags', video])
	
	pathext = os.path.splitext(video)
	outfile = pathext[0]+'_'+pathext[1]
	outfilemp4 = pathext[0]+'.mp4'
	nolang = False

	info = subprocess.check_output(['mediainfo', video])
	#print(info.decode('utf-8'))
	vidmatches = re.findall(r"Chroma subsampling\s+:\s+([0-9:]+)\s+?Bit depth\s+:\s+(\d+)", info.decode('utf-8'))
	#print(vidmatches)
	if len(vidmatches) > 0:
		if vidmatches[0][0] != "4:2:0" or vidmatches[0][1] != "8":
			reencode = vidmatches[0][0]+" chroma subsampling and "+vidmatches[0][1]+" bit (good: 4:2:0 + 8 bit)"
			#print("re-encoded needed")
	if not reencode:
		codecmatches = re.findall(r"Writing library\s+:\s+x(\d+)", info.decode('utf-8'))
		if len(codecmatches) > 0:
			if codecmatches[0] != "264":
				reencode = "x"+codecmatches[0]+" encode (good: x264)"
		#else:
		#	reencode = "non x___ encode (not x264)"
	if not reencode:
		heightmatches = re.findall(r"Height\s+:\s+([0-9 ]+)", info.decode('utf-8'))
		if len(heightmatches) > 0:
			if int(heightmatches[0].replace(" ", "")) > 720:
				reencode = heightmatches[0].replace(" ", "")+" height (good: 720)"
	# if not reencode:
		# codec2matches = re.findall(r"Codec ID\s+:\s+(\w+)", info.decode('utf-8'))
		# if len(codec2matches) > 0:
			# if codec2matches[0] != "avc1":
				# reencode = codec2matches[0]+" Codec ID (good: avc1)"
		# else:
			## reencode = "no Codec ID specified (good: avc1)"
	#if not reencode:
		#codec2matches = re.findall(r"Color range\s+:\s+(\w+)", info.decode('utf-8'))
		#if len(codec2matches) > 0:
			#if codec2matches[0] == "Limited":
				#reencode = codec2matches[0]+" colour range (good: -)"
	# AUDIO
	amatches = re.findall(r"Audio(?: #\d+)?\s+ID\s+:\s+(\d+)\s[\W\w]+?Language\s+:\s+(\w+)", info.decode('utf-8'))
	# SUBTITLES
	matches = re.findall(r"Text(?: #\d+)?\s+ID\s+:\s+(\d+)\s[\W\w]+?Count of elements\s+:\s+(\d+)\s[\W\w]+?Language\s+:\s+(\w+)", info.decode('utf-8'))
	if not matches:
		matches = re.findall(r"Text(?: #\d+)?\s+ID\s+:\s+(\d+)\s[\W\w]+?Language\s+:\s+(\w+)[\W\w]+?FromStats_FrameCount\s+:\s+(\d+)", info.decode('utf-8'))
	# No Language attribute
	if not matches:
		matches = re.findall(r"Text(?: #\d+)?\s+ID\s+:\s+(\d+)\s[\W\w]+?Count of elements\s+:\s+(\d+)", info.decode('utf-8'))
		nolang = True
	if not matches:
		matches = re.findall(r"Text(?: #\d+)?\s+ID\s+:\s+(\d+)\s[\W\w]+?FromStats_FrameCount\s+:\s+(\d+)", info.decode('utf-8'))
		nolang = True
	#print(matches)
	#print(amatches)
	
	toprocess = None
	nosub = True
	nummatches = len(matches)
	numamatches = len(amatches)
	tracktoget = None
	engtracks = 0
	atracktoget = None
	japatracks = 0
	if nummatches > 0:
		maxlines = 0
		for m in matches:
			if m[1].isdigit():
				count = int(m[1])
				if not nolang:
					lang = m[2]
				else:
					lang = "English*" # presume English
			else:
				count = int(m[2])
				lang = m[1]				
			if lang == "English" or lang == "English*" or nummatches == 1:
				engtracks += 1
				nosub = False
				if count > maxlines:
					maxlines = count
					tracktoget = m[0]
			print("--- Text ID #"+m[0]+" | Language: "+lang+" | Element Count: "+str(count))
	if numamatches > 0:
		for m in amatches:
			if m[1] == prefaudio or numamatches == 1:
				atracktoget = m[0]
			print("--- Audio ID #"+m[0]+" | Language: "+m[1])
	print(">>> Sub Track: "+tracktoget)
	print(">>> Audio Track: "+atracktoget)
	if engtracks > 0 and tracktoget and numamatches > 1 and atracktoget:
		# >>> Make optimal subtitle and audio track the only one
		subprocess.call(['mkvmerge', '-o', outfile, '--subtitle-tracks', str(int(tracktoget)-1), '--audio-tracks', str(int(atracktoget)-1), video])
		# >>> Set subtitle track as default
		subprocess.call(['mkvpropedit', outfile, '--edit', 'track:s1', '--set', 'flag-default=1', '--edit', 'track:a1', '--set', 'flag-default=1'])
		toprocess = outfile
		os.remove(video)
	elif engtracks > 0 and tracktoget:
		if nummatches > 1:
			# >>> Make optimal subtitle track the only one
			subprocess.call(['mkvmerge', '-o', outfile, '--subtitle-tracks', str(int(tracktoget)-1), video])
			# >>> Set subtitle track as default
			subprocess.call(['mkvpropedit', outfile, '--edit', 'track:s1', '--set', 'flag-default=1'])
			toprocess = outfile
			os.remove(video)
	elif numamatches > 1 and atracktoget:
		# >>> Make optimal audio track the only one
		subprocess.call(['mkvmerge', '-o', outfile, '--audio-tracks', str(int(atracktoget)-1), video])
		# >>> Set audio track as default
		subprocess.call(['mkvpropedit', outfile, '--edit', 'track:a1', '--set', 'flag-default=1'])
		toprocess = outfile
		os.remove(video)
	if reencode:
		if not toprocess:
			toprocess = video
		if tracktoget:
			activitylog.append(videoname+"\n --- Re-encoded w/ English subtitles, was "+reencode)
			subprocess.call(['ffmpeg', '-y', '-i', toprocess, '-preset', 'veryfast', '-crf', '24', '-tune', 'film', '-acodec', 'aac', '-level:v', '4.0', '-profile:v', 'high', '-pix_fmt', 'yuv420p', '-c:v', 'libx264', '-movflags', '+faststart', '-sn', '-vf', 'subtitles=\''+toprocess+'\':si=0, scale=-1:720', outfilemp4])
		else:
			activitylog.append(videoname+"\n --- Re-encoded w/o English subtitles, was "+reencode)
			subprocess.call(['ffmpeg', '-y', '-i', toprocess, '-preset', 'veryfast', '-crf', '24', '-tune', 'film', '-acodec', 'aac', '-level:v', '4.0', '-profile:v', 'high', '-pix_fmt', 'yuv420p', '-c:v', 'libx264', '-movflags', '+faststart', '-sn', '-vf', 'scale=-1:720', outfilemp4])
		if os.path.isfile(video):
			os.remove(video)
		if os.path.isfile(toprocess):
			os.remove(toprocess)
	else:
		if os.path.isfile(outfile):
			activitylog.append(videoname+"\n --- Optimized subtitles/audio")
			os.rename(outfile, video)
	if nosub:
		nosubs.append(videoname)

if len(nosubs) > 0:
	print("\n>>> "+str(len(nosubs))+" videos without any English subtitles:")
	print(*nosubs, sep = "\n")
else:
	print("\n>>> All videos have English subtitles!")

if len(activitylog) > 0:
	print("\n>>> Processed "+str(len(activitylog))+" videos:")
	print(*activitylog, sep = "\n")
	print("")
