# Andrew's Random Code Snippets

- flickr.py - a semi-automated flickr album downloader using `flickrdata.txt` as a source file, outputs links to original image quality in a `flickrurls.txt` file
- subs.py - scans `.mkv` files in the current folder and set the English subtitle track with the most elements (usually SDH) as the default and remove all other subtitle tracks
- subsclean.py - ^ remove common subtitle "credits" (e.g. "Synced and corrected by ABC")
- subsinject.py - scans `.srt` files in the current folder and softcodes the subtitles to matched `.mkv` videos (matches by S00E00 in the filename)
- subsinjectclean.py - ^ remove common subtitle "credits" (e.g. "Synced and corrected by ABC")
- subsren.py - scans `.srt` files in the current folder and renames them to matched `.mkv` videos (matches by S00E00 in the filename)
