# "Black" Standard Notes Theme

## Installation Instructions

1. Open Standard Notes
2. Click on `Extensions` in the bottom left hand corner
3. Click on `Import Extension`
4. Paste in `https://5m.ca/black`, `https://listed.to/dp8H0X66y3` or `https://gitlab.com/andryou/andrews-settings/raw/master/standardnotes/black.json`
5. Press `Enter`
6. Confirm
7. Scroll up to `Installed Extensions` and under `Black` click on `Activate`
